package system

import (
	"errors"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/controllers/garden"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblmeasurement"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblsensor"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblsystemgarden"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblsystemsensor"
)

const invalidID string = "-1"

type SystemData struct {
	SystemCreatedDate time.Time                       `json:"systemCreatedDate"`
	SensorList        []tblsensor.TblSensor           `json:"sensorList"`
	Measurements      []tblmeasurement.TblMeasurement `json:"measurements"`
}

func CreateIfNotExists(systemID string) (err error) {
	var systemGarden tblsystemgarden.TblSystemGarden
	err = database.DB.
		Where("systemid = ?", systemID).
		Find(&systemGarden).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		log.Printf("Error retrieving systemgarden: %v", err)
		return
	}
	if systemGarden.GardenID != "" {
		// Cool, garden exists I guess!
		return nil
	}

	_, err = garden.CreateNewGarden(systemID)

	return err
}

// This endpoint is used to check if there exists measurements
// for a given system, but a garden hasn't claimed usage of that
// system yet. A requirement for a user claiming a system to
// create a garden.
func GetSystemExistsButNotGarden(c *gin.Context) {
	id := c.Param("id")
	if len(id) == 0 {
		c.JSON(http.StatusNotFound, gin.H{"data": "System with ID " + id + " Not found"})
		return
	}

	result, err := garden.CheckSystemExistsButNotGarden(id)

	if result {
		// This is what we want, the date hasn't been set up yet.
		c.JSON(http.StatusOK, gin.H{"data": "System with ID " + id + " is available"})
	} else {
		c.JSON(http.StatusNotFound, gin.H{
			"data": err.Error(),
		})
	}
	return
}

func getSystemData(c *gin.Context, id string) {
	// Get created date of system
	var systemGarden tblsystemgarden.TblSystemGarden
	err := database.DB.
		Where("systemid = ?", id).
		Find(&systemGarden).Error
	if err != nil {
		log.Printf("Error retrieving systemgarden: %v", err)
		c.JSON(http.StatusInternalServerError, gin.H{"data": "Error fetching system: " + err.Error()})
		return
	}

	var sensorIDs []string
	var systemSensors []tblsystemsensor.TblSystemSensor
	var sensors []tblsensor.TblSensor
	var measurements []tblmeasurement.TblMeasurement
	// Get sensor IDs in system
	err = database.DB.
		Where("systemid = ?", id).
		Find(&systemSensors).Error
	if err != nil {
		log.Printf("Error retrieving sensors for system: %v", err)
		c.JSON(http.StatusInternalServerError, gin.H{"data": "Error retrieving sensors for system " + err.Error()})
		return
	}

	if len(systemSensors) > 0 {
		for i := 0; i < len(systemSensors); i++ {
			sensorIDs = append(sensorIDs, systemSensors[i].SensorID)
		}
		err = database.DB.
			Where("sensorid IN (?)", sensorIDs).
			Find(&sensors).Error
		if err != nil {
			log.Printf("Error retrieving sensors data: %v", err)
			c.JSON(http.StatusInternalServerError, gin.H{"data": "Error retrieving sensor data " + err.Error()})
			return
		}

		thresholdTime := time.Now()
		// Get data for the last 7 days?
		thresholdTime = thresholdTime.AddDate(0, 0, -7)
		err = database.DB.
			Where("sensorid IN (?)", sensorIDs).
			Where("timestamp > ?", thresholdTime).
			Order("sensorid").
			Order("timestamp desc").
			Find(&measurements).Error
		if err != nil {
			log.Printf("Error retrieving measurement data: %v", err)
			c.JSON(http.StatusInternalServerError, gin.H{"data": "Error retrieving measurement data " + err.Error()})
			return
		}
	}
	result := SystemData{
		SystemCreatedDate: systemGarden.CreatedDate,
		SensorList:        sensors,
		Measurements:      measurements,
	}
	c.JSON(http.StatusOK, gin.H{"data": result})
}
