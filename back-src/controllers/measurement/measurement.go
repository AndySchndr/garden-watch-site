package measurement

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/controllers/garden"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/controllers/sensor"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/controllers/system"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblmeasurement"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblsensor"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblsystemgarden"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblsystemsensor"
)

type requestBody struct {
	SYSTEMID     string            `json:"sysid" binding:"required"`
	MEASUREMENTS []measurementData `json:"meas" binding:"required"`
}

type measurementData struct {
	SensorID string  `json:"sensorid"`
	Value    float32 `json:"val" binding:"Required"`
	Units    string  `json:"unit" binding:"Required"`
	Label    string  `json:"label" binding:"Required"` // Default label for the system.
}

type sensorData struct {
	SensorID     string                          `json:"sensorid"`
	Label        string                          `json:"label"`
	Units        string                          `json:"unit"`
	Measurements []tblmeasurement.TblMeasurement `json:"measurements"`
}

// POST
func Post(c *gin.Context) {
	var request requestBody
	if err := c.ShouldBindJSON(&request); err != nil {
		log.Println("Error with measurement data input: %v", err)
		c.JSON(http.StatusBadRequest, gin.H{"data": "Error saving measurements: " + err.Error()})
		return
	}
	fmt.Printf("Got measurement request: %v", request)
	if len(request.SYSTEMID) == 0 {
		log.Print("Error with measurements - no system ID provided")
		c.JSON(http.StatusBadRequest, gin.H{"data": "Missing system ID data"})
		return
	}
	if len(request.MEASUREMENTS) == 0 {
		log.Print("Error with measurements - no measurements.")
		c.JSON(http.StatusBadRequest, gin.H{"data": "Missing measurement data"})
		return
	}

	var err error
	// Check to make sure there is a garden for this system
	err = system.CreateIfNotExists(request.SYSTEMID)
	if err != nil {
		log.Printf("Error getting or creating garden for incoming measurement: %v", err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"data": " Error finding system for measurement ",
		})
		return

	}
	errCount := 0
	succCount := 0
	for i := 0; i < len(request.MEASUREMENTS); i++ {
		processErr := processMeasurement(request.SYSTEMID, request.MEASUREMENTS[i])
		if processErr != nil {
			if err == nil {
				err = errors.New("Error processing measurements: ")
			}
			errCount += 1
			err = fmt.Errorf("%v\n%v", err, processErr)
		} else {
			succCount += 1
		}
	}
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"data": strconv.Itoa(succCount) + " Measurements successfully saved, " + strconv.Itoa(errCount) + " Errors: " + err.Error(),
		})
		log.Printf("%v", err)
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"data": strconv.Itoa(succCount) + " Measurements successfully saved, " + strconv.Itoa(errCount) + " Errors",
	})
	return
}

// Check if the systemID is new, if so, set that up
// Check if the sensor is new to the system, if so set that up
// Add the measurement to the database
func processMeasurement(
	systemID string,
	meas measurementData,
) (err error) {

	// set up the sensorID
	if meas.SensorID == "" {
		meas.SensorID = systemID + meas.Label
	}

	// Check if the sensor is new to the system, save if so
	err = database.DB.
		Where(tblsystemsensor.TblSystemSensor{
			SystemID: systemID,
			SensorID: meas.SensorID,
		}).
		FirstOrCreate(&tblsystemsensor.TblSystemSensor{}).Error
	if err != nil {
		return
	}

	// Check if the sensor is new, save if so. assume displayed units same as given units.
	err = database.DB.
		Where(tblsensor.TblSensor{
			SensorID: meas.SensorID,
		}).
		Attrs(tblsensor.TblSensor{
			StoreUnits:   meas.Units,
			DisplayUnits: meas.Units,
			Label:        meas.Label, // should use OG label if not exists
		}).
		FirstOrCreate(&tblsensor.TblSensor{}).Error
	if err != nil {
		return
	}

	// Record measurement
	measurement := tblmeasurement.TblMeasurement{
		SensorID:    meas.SensorID,
		TimeStamp:   time.Now(),
		Measurement: meas.Value,
	}
	err = database.DB.Create(&measurement).Error
	return
}

func Get(c *gin.Context) {
	sensorID := c.Param("id")
	if len(sensorID) == 0 {
		log.Printf("id missing from request")
		c.JSON(http.StatusBadRequest, gin.H{"data": "id missing from request"})
		return
	}
	startDateStr := c.Param("start")
	if len(startDateStr) == 0 {
		log.Printf("start date missing from request")
		c.JSON(http.StatusBadRequest, gin.H{"data": "start date missing from request"})
		return
	}
	startDate, err := parseMeasDate(startDateStr)
	if err != nil {
		err = fmt.Errorf("start date format error: %v", err.Error())
		log.Printf(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
		return
	}

	endDateStr := c.Param("end")
	if len(endDateStr) == 0 {
		log.Printf("end date missing from request")
		c.JSON(http.StatusBadRequest, gin.H{"data": "end date missing from request"})
		return
	}
	endDate, err := parseMeasDate(endDateStr)
	endDate = endDate.Add(time.Hour*24 - time.Minute)
	if err != nil {
		err = fmt.Errorf("end date format error: %v", err.Error())
		log.Printf(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
		return
	}

	result, err := getSensorData(sensorID, startDate, endDate)
	if err != nil {
		err = fmt.Errorf("error getting sensor data: %v", err.Error())
		log.Printf(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"data": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": result})
}

func parseMeasDate(dateStr string) (date time.Time, err error) {
	date, err = time.Parse("2006-01-02", dateStr)
	return
}

// TODO: Check that the sensor belongs to a system which belongs to
// a garden which is owned by the current user, or public
func getSensorData(
	sensorID string,
	start time.Time,
	end time.Time,
) (
	result sensorData,
	err error,
) {
	// Find the garden connected to this sensor

	gardens := []tblsystemgarden.TblSystemGarden{}
	err = database.DB.Model(&tblsystemgarden.TblSystemGarden{}).
		Select("tblsystemgarden.gardenid").
		Joins("left join tblsystemsensor ON tblsystemsensor.systemid = tblsystemgarden.systemid").
		Where("tblsystemsensor.sensorid = ?", sensorID).
		Scan(&gardens).Error
	if err != nil || len(gardens) == 0 {
		err = fmt.Errorf("Error Getting Garden for sensor %s - %v", sensorID, err)
		log.Printf(err.Error())
		return
	}

	// check that the garden is public or owned by the current user
	garden, err := garden.GetGarden(gardens[0].GardenID)
	if err != nil {
		err = fmt.Errorf("Error Getting Garden for sensor %s - %v", sensorID, err)
		log.Printf(err.Error())
		return
	}
	// TODO: Check that garden is owned by current user if private
	if !garden.Public {
		err = fmt.Errorf("Error Getting Garden for sensor %s - Garden is not public", sensorID)
		log.Printf(err.Error())
		return
	}

	// Get the data for this sensor
	sensor, err := sensor.GetSensorByID(sensorID)
	if err != nil {
		err = fmt.Errorf("Error Getting data for sensor %s - %v", sensorID, err)
		log.Printf(err.Error())
		return
	}

	// TODO - if sensorunits don't match displayunits, then convert the measurements

	result.Label = sensor.Label
	result.Units = sensor.DisplayUnits
	result.SensorID = sensorID

	// Get all the measurements
	database.DB.Model(&tblmeasurement.TblMeasurement{}).
		Select("tblmeasurement.timestamp, tblmeasurement.measurement").
		Where("sensorid = ?", sensorID).
		Where("timestamp BETWEEN ? AND ?", start, end).
		Order("timestamp asc").
		Find(&result.Measurements)

	if err != nil {
		err = fmt.Errorf("Error Getting data for sensor %s - %v", sensorID, err)
		log.Printf(err.Error())
	}

	return
}
