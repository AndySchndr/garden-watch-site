package sensor

import (
	"log"

	"gitlab.com/AndySchndr/garden-watch-site/back-src/database"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblsensor"
)

func GetSensorByID(sensorID string) (sensor tblsensor.TblSensor, err error) {
	err = database.DB.
		Where("sensorid = ?", sensorID).
		Find(&sensor).Error
	if err != nil {
		log.Printf("Error finding sensor with ID: %s: %v", sensorID, err)
	}
	return
}
