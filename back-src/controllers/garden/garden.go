package garden

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	guuid "github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblgarden"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblsystemgarden"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblsystemsensor"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblusergarden"
)

type claimRequestBody struct {
	SystemID    string `json:"systemid"`
	Name        string `json:"name"`
	CuteLink    string `json:"cutelink"`
	Description string `json:"description"`
	Public      bool   `json:"public"`
	UserID      string `json:"userid"` // This might not be necessary. could just use cookie.
}

var isValidNickname = regexp.MustCompile(`^[a-z,0-9,-]+$`).MatchString

type gardenData struct {
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Public      bool     `json:"public"`
	CuteLink    string   `json:"cutelink"`
	SensorIDs   []string `json:"sensorids"`
}

// Get all the data for a garden.
// TODO: Make sure that if the garden is private, only
// complete the request if the current user owns the garden.
func Get(c *gin.Context) {
	link := c.Param("link")
	if len(link) > 0 {
		// Try to get the data for a specific system
		GetGardenData(c, link)
		return
	}
	c.JSON(http.StatusBadRequest, gin.H{"data": "Error getting garden data: invalid link"})
	return
}

// Given a system ID, a name cute link, description, public setting,
// User ID,
// Allow a user to Claim a garden as their own.
// should include a check to make sure that user is logged in later??
// check that the token in the cookie matches the user ID?
func Claim(c *gin.Context) {
	var request claimRequestBody
	if err := c.ShouldBindJSON(&request); err != nil {
		log.Println("Error with claim input: %v", err)
		c.JSON(http.StatusBadRequest, gin.H{"data": "Error claiming garden: " + err.Error()})
		return
	}
	if len(request.SystemID) == 0 ||
		len(request.Name) == 0 ||
		len(request.CuteLink) == 0 ||
		len(request.UserID) == 0 {
		log.Println("Error with claim input, missing data: %v", request)
		c.JSON(http.StatusBadRequest, gin.H{"data": "Missing data"})
		return
	}

	request.CuteLink = strings.ToLower(request.CuteLink)
	if !isValidNickname(request.CuteLink) {
		err := fmt.Errorf("Error Claiming Garden: Link is not valid %s", request.CuteLink)
		log.Println(err)
		c.JSON(http.StatusInternalServerError, gin.H{"data": err.Error()})
		return
	}

	// Check to make sure there is a garden for this system
	available, err := CheckSystemExistsButNotGarden(request.SystemID)
	if err != nil || !available {
		err = fmt.Errorf("Error Claiming Garden: %v", err)
		log.Println(err)
		c.JSON(http.StatusInternalServerError, gin.H{"data": err.Error()})
		return
	}

	garden, err := GetGardenFromSystemID(request.SystemID)
	if err != nil || !available {
		err = fmt.Errorf("Error Claiming Garden, not found in db")
		log.Println(err)
		c.JSON(http.StatusInternalServerError, gin.H{"data": err.Error()})
		return
	}

	// update the hell out of tblGarden entry
	garden.Name = request.Name
	garden.Public = request.Public // False until the garden is 'claimed'
	garden.CreatedDate = time.Now()
	garden.Description = request.Description
	garden.CuteURL = request.CuteLink

	err = database.DB.
		Model(&tblgarden.TblGarden{}).
		Where("gardenid = ?", garden.GardenID).
		Update(&garden).Error
	if err != nil {
		log.Println("Error saving garden while claiming: %v", err)
		err = fmt.Errorf("Error Claiming Garden, database error")
		c.JSON(http.StatusInternalServerError, gin.H{"data": err.Error()})
		return
	}

	// add an entry to tblUserGarden
	userGarden := tblusergarden.TblUserGarden{
		GardenID: garden.GardenID,
		UserID:   request.UserID,
	}
	err = database.DB.Create(&userGarden).Error
	if err != nil {
		log.Println("Error linking garden to user: %v", err)
		err = fmt.Errorf("Error Claiming Garden, could not link garden to user")
		c.JSON(http.StatusInternalServerError, gin.H{"data": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": "Success!"})
	return

}

func CheckSystemExistsButNotGarden(id string) (result bool, err error) {
	var systemGarden tblsystemgarden.TblSystemGarden
	err = database.DB.
		Where("systemid = ?", id).
		Find(&systemGarden).Error
	if err != nil {
		log.Printf("Error retrieving systemgarden %s: %v", id, err)
		err = fmt.Errorf("System with ID " + id + " not found")
		return
	}

	// Ok so we found a system-garden linkage,
	// make sure that garden is 'unclaimed'
	garden, err := GetGarden(systemGarden.GardenID)
	if err != nil {
		log.Printf("Error retrieving garden: %v", err)
		err = fmt.Errorf("System with ID " + id + " not found")
		return
	}

	if !garden.CreatedDate.IsZero() {
		log.Printf("Garden %s has a non-zero date.", garden.GardenID)
		err = fmt.Errorf("System with ID " + id + " is already in use")
		return
	}
	result = true
	return
}

func CreateNewGarden(systemID string) (gardenID string, err error) {
	uuidWithHyphen := guuid.New()
	fmt.Println(uuidWithHyphen)
	gardenID = strings.Replace(uuidWithHyphen.String(), "-", "", -1)
	log.Printf("Generated ID for new garden: %s\n", gardenID)

	garden := tblgarden.TblGarden{
		GardenID:    gardenID,
		Name:        "",
		Public:      false, // False until the garden is 'claimed'
		Description: "",
		CuteURL:     "",
	}
	err = database.DB.Create(&garden).Error
	if err != nil {
		log.Printf("Error creating new garden %v\n", err)
		return "", err
	}

	newSystem := tblsystemgarden.TblSystemGarden{
		SystemID:    systemID,
		CreatedDate: time.Now(),
		GardenID:    gardenID,
	}
	err = database.DB.Create(&newSystem).Error
	if err != nil {
		log.Printf("Error creating new system %v\n", err)
		return "", err
	}
	return gardenID, nil
}

func GetGardenFromSystemID(systemID string) (garden tblgarden.TblGarden, err error) {
	systemGarden := tblsystemgarden.TblSystemGarden{}
	err = database.DB.
		Where("systemid = ?", systemID).
		Find(&systemGarden).Error
	if err != nil {
		log.Printf("Error finding garden linked to system ID %s: %v", systemID, err)
		return
	}

	garden, err = GetGarden(systemGarden.GardenID)
	return
}

func GetGarden(gardenID string) (garden tblgarden.TblGarden, err error) {
	err = database.DB.
		Where("gardenid = ?", gardenID).
		Find(&garden).Error
	if err != nil {
		log.Printf("Error finding garden with ID: %s: %v", gardenID, err)
	}
	return
}

func GetLinkAvailable(c *gin.Context) {
	link := c.Param("link")
	if len(link) > 0 {
		// Check if we can find a garden with this link
		_, err := GetGardenByLink(link)
		if errors.Is(err, gorm.ErrRecordNotFound) {
			// Great! I guess
			c.JSON(http.StatusOK, gin.H{"data": true, "msg": "Link is available"})
		}
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": false, "msg": "Empty link is also invalid"})
	return
}

func GetGardenByLink(gardenLink string) (garden tblgarden.TblGarden, err error) {
	err = database.DB.
		Where("cuteurl = ?", gardenLink).
		Find(&garden).Error
	if err != nil {
		log.Printf("Error finding garden with link: %s: %v", gardenLink, err)
	}
	return
}

// Returns the data for a garden and the IDs for all
// the sensors that are connected to the garden.
// TODO: check that the garden is public or owned by
// the current user.
func GetGardenData(c *gin.Context, gardenLink string) {
	garden, err := GetGardenByLink(gardenLink)
	if err != nil {
		err = fmt.Errorf("Error Getting Garden by link %s", gardenLink)
		c.JSON(http.StatusBadRequest, gin.H{"data": err})
		return
	}
	if !garden.Public {
		err = fmt.Errorf("Error Getting Garden %s - Not Public", gardenLink)
		log.Printf(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
		return
	}

	systemSensors := []tblsystemsensor.TblSystemSensor{}
	err = database.DB.Model(&tblsystemsensor.TblSystemSensor{}).
		Select("tblsystemsensor.sensorid").
		Joins("left join tblsystemgarden ON tblsystemgarden.systemid = tblsystemsensor.systemid").
		Where("tblsystemgarden.gardenid = ?", garden.GardenID).
		Scan(&systemSensors).Error
	if err != nil || len(systemSensors) == 0 {
		err = fmt.Errorf("Error Getting Sensors for Garden %s - %v", gardenLink, err)
		log.Printf(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
		return
	}

	ids := []string{}
	for _, sensor := range systemSensors {
		ids = append(ids, sensor.SensorID)
	}

	result := gardenData{
		Name:        garden.Name,
		Description: garden.Description,
		Public:      garden.Public,
		CuteLink:    garden.CuteURL,
		SensorIDs:   ids,
	}
	c.JSON(http.StatusOK, gin.H{"data": result})
}
