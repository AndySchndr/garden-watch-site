package echo

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Get(c *gin.Context) {
	msg := c.Param("msg")
	log.Printf("Got request with msg: %s\n", msg)
	log.Println(c.Request)
	c.JSON(http.StatusOK, gin.H{"message": msg})
	return
}
