package tblversion

import (
	"log"

	"github.com/jinzhu/gorm"
)

type TblVersion struct {
	// gorm.Model
	ID      int `gorm:"column:id"`
	Version int `gorm:"column:version"`
}

func (TblVersion) TableName() string {
	return "tblversion"
}

// GetDBVersion gets the migration version of the database. fatals on error.
func GetDBVersion(db *gorm.DB) int {
	var ver TblVersion
	err := db.Find(&ver).Error
	if err != nil {
		log.Fatalf("Failed to get db version: %v\n", err)
	}
	return ver.Version
}

// UpdateDBVersion is a helper function for updating the database version.
// fatals on error.
func UpdateDBVersion(db *gorm.DB, version int) {
	var ver TblVersion
	err := db.Find(&ver).Error
	if err != nil {
		log.Fatalf("Failed to fetch version while updating db version: %v\n", err)
	}

	ver.Version = version
	err = db.
		Model(&TblVersion{}).
		Where("id = ?", ver.ID).
		Update(&ver).Error

	if err != nil {
		log.Fatalf("Failed to update db version: %v\n", err)
	}
}
