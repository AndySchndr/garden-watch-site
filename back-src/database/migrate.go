package database

import (
	"log"

	"github.com/lib/pq"

	"github.com/jinzhu/gorm"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblgarden"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblmeasurement"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblsensor"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblsystemgarden"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblsystemsensor"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tbluser"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblusergarden"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database/tblversion"
)

func runMigrations(db *gorm.DB) {
	migration0(db)
	migration1(db)
	migration2(db)
	migration3(db)
	migration4(db)
	migration5(db)
	migration6(db)
	migration7(db)
}

// Migration 0
// Try to Create the version table.
// If success, initialize it with 0, 0
func migration0(db *gorm.DB) {
	migration := 0
	// Try to create the table, to keep track of migrations, as our 0th migration.
	statement := `CREATE TABLE ` + tblversion.TblVersion{}.TableName() + ` (
		id SERIAL PRIMARY KEY,
		version INT
	);`
	err := db.Exec(statement).Error
	errCode := ""
	if err, ok := err.(*pq.Error); ok {
		// Here err is of type *pq.Error, you may inspect all its fields, e.g.:
		errCode = err.Code.Name()
	}
	if errCode == "duplicate_table" {
		// log.Printf("Error creating version table: %v\nassume it already exists\n", err)
		log.Printf("Migration %d complete", migration)
		return
	} else if err != nil {
		log.Fatalf("ERROR creating version table %v\n", err)
	}

	statement = `INSERT INTO ` +
		tblversion.TblVersion{}.TableName() + ` (id, version) ` +
		`VALUES (1, 0);`
	err = db.Exec(statement).Error
	if err != nil {
		log.Fatalf("ERROR initializing version table %v\n", err)
	}
	tblversion.UpdateDBVersion(db, migration)
	log.Printf("Migration %d executed", migration)
}

// Create tblGarden
func migration1(db *gorm.DB) {
	// get version
	// if version < 1, run update, update to version 1.
	migration := 1
	ver := tblversion.GetDBVersion(db)
	if ver >= migration {
		log.Printf("Migration %d complete", migration)
		return
	}
	statement := `CREATE TABLE ` + tblgarden.TblGarden{}.TableName() + ` (
		gardenid text PRIMARY KEY,
		name text,
		public bool,
		createddate timestamptz,
		description text,
		cuteurl text
		);`
	err := db.Exec(statement).Error
	if err != nil {
		log.Fatalf("Error creating tblgarden: %v\n", err)
	}
	tblversion.UpdateDBVersion(db, migration)
	log.Printf("Migration %d executed", migration)
}

// Create tblUser
func migration2(db *gorm.DB) {
	migration := 2
	// if version < migration, run update, update version.
	ver := tblversion.GetDBVersion(db)
	if ver >= migration {
		log.Printf("Migration %d complete", migration)
		return
	}
	statement := `CREATE TABLE ` + tbluser.TblUser{}.TableName() + ` (
		userid text PRIMARY KEY,
		oauthprovider text,
		nickname text,
		createddate timestamptz,
		description text
		);`
	err := db.Exec(statement).Error
	if err != nil {
		log.Fatalf("Error creating tbluser: %v\n", err)
	}
	tblversion.UpdateDBVersion(db, migration)
	log.Printf("Migration %d executed", migration)
}

// Create tblUserGarden
func migration3(db *gorm.DB) {
	migration := 3
	// if version < migration, run update, update version.
	ver := tblversion.GetDBVersion(db)
	if ver >= migration {
		log.Printf("Migration %d complete", migration)
		return
	}
	statement := `CREATE TABLE ` + tblusergarden.TblUserGarden{}.TableName() + ` (
		userid text,
		gardenid text
		);`
	err := db.Exec(statement).Error
	if err != nil {
		log.Fatalf("Error creating tblusergarden: %v\n", err)
	}
	tblversion.UpdateDBVersion(db, migration)
	log.Printf("Migration %d executed", migration)
}

// Create tblSystemGarden
func migration4(db *gorm.DB) {
	migration := 4
	// if version < migration, run update, update version.
	ver := tblversion.GetDBVersion(db)
	if ver >= migration {
		log.Printf("Migration %d complete", migration)
		return
	}
	statement := `CREATE TABLE ` + tblsystemgarden.TblSystemGarden{}.TableName() + ` (
		systemid text PRIMARY KEY,
		createddate timestamptz,
		gardenid text
		);`
	err := db.Exec(statement).Error
	if err != nil {
		log.Fatalf("Error creating tblsystemgarden: %v\n", err)
	}
	tblversion.UpdateDBVersion(db, migration)
	log.Printf("Migration %d executed", migration)
}

// Create tblSystemSensor
func migration5(db *gorm.DB) {
	migration := 5
	// if version < migration, run update, update version.
	ver := tblversion.GetDBVersion(db)
	if ver >= migration {
		log.Printf("Migration %d complete", migration)
		return
	}
	statement := `CREATE TABLE ` + tblsystemsensor.TblSystemSensor{}.TableName() + ` (
		systemid text,
		sensorid text
		);`
	err := db.Exec(statement).Error
	if err != nil {
		log.Fatalf("Error creating tblsystemsensor: %v\n", err)
	}
	tblversion.UpdateDBVersion(db, migration)
	log.Printf("Migration %d executed", migration)
}

// Create tblSensor
func migration6(db *gorm.DB) {
	migration := 6
	// if version < migration, run update, update version.
	ver := tblversion.GetDBVersion(db)
	if ver >= migration {
		log.Printf("Migration %d complete", migration)
		return
	}
	statement := `CREATE TABLE ` + tblsensor.TblSensor{}.TableName() + ` (
		sensorid text PRIMARY KEY,
		storeunits text,
		displayunits text,
		label text
		);`
	err := db.Exec(statement).Error
	if err != nil {
		log.Fatalf("Error creating tblsensor: %v\n", err)
	}
	tblversion.UpdateDBVersion(db, migration)
	log.Printf("Migration %d executed", migration)
}

// Create tblMeasurement
func migration7(db *gorm.DB) {
	migration := 7
	// if version < migration, run update, update version.
	ver := tblversion.GetDBVersion(db)
	if ver >= migration {
		log.Printf("Migration %d complete", migration)
		return
	}
	statement := `CREATE TABLE ` + tblmeasurement.TblMeasurement{}.TableName() + ` (
		sensorid text,
		timestamp timestamptz,
		measurement real
		);`
	err := db.Exec(statement).Error
	if err != nil {
		log.Fatalf("Error creating tblsensor: %v\n", err)
	}
	tblversion.UpdateDBVersion(db, migration)
	log.Printf("Migration %d executed", migration)
}

// // Add the Blog post "Photon - Per-Region Player Counts in Real Time"
// func migration5(db *gorm.DB) {
// 	migration := 5
// 	// if version < migration, run update, update version.
// 	ver := tblversion.GetDBVersion(db)
// 	if ver >= migration {
// 		log.Printf("Migration %d complete", migration)
// 		return
// 	}
// 	newArticle := tblarticle.TblArticle{
// 		Title:       "Photon - Per-Region Player Counts in Real Time",
// 		Path:        "photon-region-player-counts",
// 		PageName:    "photonregionplayercounts",
// 		PostType:    tblarticle.Blog,
// 		Enabled:     true,
// 		HeaderImg:   "photonregionheader.jpg",
// 		CreatedDate: time.Now(),
// 		UpdatedDate: time.Now(),
// 		Description: "Photon is an amazing tool when making multiplayer games in Unity, but my need for a simple-sounding feature sent me on an unexpected quest...",
// 	}
// 	result := db.Create(&newArticle)
// 	if result.Error != nil {
// 		log.Fatalf("Error executing migration %d: %v\n", migration, result.Error)
// 	}
// 	tblversion.UpdateDBVersion(db, migration)
// 	log.Printf("Migration %d executed", migration)
// }
