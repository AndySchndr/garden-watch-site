package tbluser

// Defines a User! Will be filled out later after I learn more about auth haha

import (
	"time"
)

const Article = "Article"
const Blog = "Blog"

type TblUser struct {
	// The userID should be like an ID from an oauth provider
	UserID        string    `gorm:"column:userid"`
	OAuthProvider string    `gorm:"column:oauthprovider"`
	NickName      string    `gorm:"column:nickname"`
	CreatedDate   time.Time `gorm:"column:createddate"`
	Description   string    `gorm:"column:description"`
}

func (TblUser) TableName() string {
	return "tbluser"
}
