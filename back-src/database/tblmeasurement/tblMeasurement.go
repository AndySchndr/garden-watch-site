package tblmeasurement

import "time"

// Records measurements as part of a system!

type TblMeasurement struct {
	// gorm.Model
	SensorID    string    `gorm:"column:sensorid"`
	TimeStamp   time.Time `gorm:"column:timestamp"`
	Measurement float32   `gorm:"column:measurement"`
}

func (TblMeasurement) TableName() string {
	return "tblmeasurement"
}

// GetMeasurements for a given sensor ID
