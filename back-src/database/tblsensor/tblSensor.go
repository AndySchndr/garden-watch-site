package tblsensor

// Defines the ID, units, units used for display, and label for a sensor.

type TblSensor struct {
	// gorm.Model
	SensorID     string `gorm:"column:sensorid"`
	StoreUnits   string `gorm:"column:storeunits"`
	DisplayUnits string `gorm:"column:displayunits"`
	Label        string `gorm:"column:label"`
}

func (TblSensor) TableName() string {
	return "tblsensor"
}
