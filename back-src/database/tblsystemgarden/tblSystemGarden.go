package tblsystemgarden

import "time"

// Basically a table for uniquely identifying sytems.
// A system can only belong to one garden, and has
// a created date.

type TblSystemGarden struct {
	// gorm.Model
	SystemID    string    `gorm:"column:systemid"`
	CreatedDate time.Time `gorm:"column:createddate"`
	GardenID    string    `gorm:"column:gardenid"`
}

func (TblSystemGarden) TableName() string {
	return "tblsystemgarden"
}
