package tblusergarden

// Defines relationships between a user and gardens

type TblUserGarden struct {
	// gorm.Model
	UserID   string `gorm:"column:userid"`
	GardenID string `gorm:"column:gardenid"`
}

func (TblUserGarden) TableName() string {
	return "tblusergarden"
}
