package tblsystemsensor

// Defines which system a sensor belongs to!

type TblSystemSensor struct {
	// gorm.Model
	SystemID string `gorm:"column:systemid"`
	SensorID string `gorm:"column:sensorid"`
}

func (TblSystemSensor) TableName() string {
	return "tblsystemsensor"
}
