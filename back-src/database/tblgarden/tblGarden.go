package tblgarden

import (
	"time"
)

type TblGarden struct {
	GardenID    string    `gorm:"column:gardenid"`
	Name        string    `gorm:"column:name"`
	Public      bool      `gorm:"column:public"`
	CreatedDate time.Time `gorm:"column:createddate"`
	Description string    `gorm:"column:description"`
	CuteURL     string    `gorm:"column:cuteurl"`
}

func (TblGarden) TableName() string {
	return "tblgarden"
}
