package database

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/lib/pq"
)

type dbInfo struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	Dbname   string `json:"dbname"`
}

const connectDelay time.Duration = 5

var DB *gorm.DB

// ConnectDatabase ensures that the db exists and is up to date.
func ConnectDatabase(local bool) {
	dbInfoPath := "dbinfo.json"
	if !local {
		dbInfoPath = "/var/www/gardenwatch.ca/dbinfo.json"
	}
	dbInfo, err := getDBInfo(dbInfoPath)
	if err != nil {
		log.Fatalf("Error loading database info: %v", err)
	}
	connected := false
	var db *gorm.DB
	for !connected {
		db, err = connectOrCreate(dbInfo)
		if err != nil {
			log.Printf("Error connecting to db, trying again in %d seconds. %v\n", connectDelay, err)
			time.Sleep(connectDelay * time.Second)
		} else {
			connected = true
		}
	}

	log.Print("Running database migrations...")
	runMigrations(db)
	log.Print("Done")

	DB = db
}

func getDBInfo(jsonFileName string) (info dbInfo, err error) {
	// Open our jsonFile
	jsonFile, err := os.Open(jsonFileName)
	// if we os.Open returns an error then handle it
	if err != nil {
		log.Fatalf("Error opening DB info json file: %v", err)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	err = json.Unmarshal(byteValue, &info)
	if err != nil {
		log.Fatalf("Error unmarshaling DB info json file: %v", err)
	}
	return
}

func connectOrCreate(info dbInfo) (*gorm.DB, error) {
	var db *gorm.DB

	psqlInfo := fmt.Sprintf("user=%s password=%s dbname=postgres port=%d host=%s sslmode=disable",
		info.User, info.Password, info.Port, info.Host)
	db, err := gorm.Open("postgres", psqlInfo)
	if err != nil {
		log.Printf("Error opening db: %v\n", err)
		return db, err
	}
	err = db.Exec("CREATE DATABASE " + info.Dbname + ";").Error

	errCode := ""
	if err, ok := err.(*pq.Error); ok {
		// Here err is of type *pq.Error, you may inspect all its fields, e.g.:
		errCode = err.Code.Name()
	}
	if errCode == "duplicate_database" {
		log.Printf("DB already exists, trying to connect...\n")
		err = nil
	} else if err == nil {
		log.Printf("Created DB, trying to connect...\n")
	}
	if err != nil {
		log.Fatalf("Error creating database: %v", db.Error)
	}

	psqlInfo = fmt.Sprintf("user=%s password=%s dbname=%s port=%d host=%s sslmode=disable",
		info.User, info.Password, info.Dbname, info.Port, info.Host)
	db, err = gorm.Open("postgres", psqlInfo)
	if err != nil {
		log.Printf("Unable to connect to database %v\n", err)
	}

	db.LogMode(flag.Lookup("debugdb").Value.(flag.Getter).Get().(bool))
	return db, err
}
