package main

import (
	"flag"
	"log"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/secure"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/controllers/echo"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/controllers/garden"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/controllers/measurement"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/controllers/system"
	"gitlab.com/AndySchndr/garden-watch-site/back-src/database"
)

var local = flag.Bool("local", false, "whether the server is being run locally or in deployment. determines where to look for react build.")
var debugdb = flag.Bool("debugdb", false, "whether to debug GORM")

func main() {
	flag.Parse()
	// Set the router as the default one shipped with Gin
	router := gin.Default()
	log.Print("Configuring router...")
	// CORS for https://gardenwatch..ca and http://localhost:5000 (if local) origins, allowing:
	// - Any methods
	// - Origin header
	// - Credentials share
	// - Preflight requests cached for 12 hours

	router.Use(cors.New(cors.Config{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"GET", "POST"},
		AllowHeaders: []string{
			"Content-Type",
			"Content-Length",
			"Accept-Encoding",
			"X-CSRF-Token",
			"Authorization",
			"accept",
			"origin",
			"Cache-Control",
			"X-Requested-With",
		},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowOriginFunc: func(origin string) bool {
			return origin == "https://gardenwatch.ca" ||
				*local && origin == "http://localhost:3000" // port used when hosting website locally
		},
		MaxAge: 12 * time.Hour,
	}))

	// Setup route group for the API
	api := router.Group("/api")
	{
		api.GET("/systemavailable/:id", system.GetSystemExistsButNotGarden)
		api.POST("/measurement", measurement.Post)
		api.GET("/echo/:msg", echo.Get)
		api.POST("/garden/claim", garden.Claim)
		api.GET("/garden/:link", garden.Get)
		api.GET("/gardenlinkavailable/:link", garden.GetLinkAvailable)
		api.GET("/sensor/:id/:start/:end", measurement.Get)
	}

	// Serve frontend static files
	if *local {
		router.Use(static.Serve("/", static.LocalFile("./../front-src/build", true)))
		router.NoRoute(func(c *gin.Context) {
			c.File("./../front-src/build/index.html")
		})
		log.Print("running locally, looking for build in ./front-src/build")
	} else {
		router.Use(static.Serve("/", static.LocalFile("/var/www/gardenwatch.ca/html", true)))
		router.NoRoute(func(c *gin.Context) {
			c.File("/var/www/gardenwatch.ca/html/index.html")
		})
		log.Print("running production, looking for build in /var/www/gardenwatch.ca/html")
	}

	database.ConnectDatabase(*local)

	useHTTP := true

	err := router.SetTrustedProxies(nil)
	if err != nil {
		log.Fatalf("Error setting trusted proxies: %v")
	}

	// Start and run the server
	if *local || useHTTP {
		// No certificates
		router.Run(":5000")
	} else {
		router.Use(LoadTls())
		router.RunTLS(
			":5000",
			"/etc/letsencrypt/live/gardenwatch.ca/cert.pem",
			"/etc/letsencrypt/live/gardenwatch.ca/privkey.pem",
		)
	}
}

func LoadTls() gin.HandlerFunc {
	// return func(c *gin.Context) {
	config := secure.New(secure.Config{
		AllowedHosts:          []string{"gardenwatch.ca", "ssl.gardenwatch.ca"},
		SSLRedirect:           true,
		SSLHost:               "ssl.gardenwatch.ca",
		STSSeconds:            315360000,
		STSIncludeSubdomains:  true,
		FrameDeny:             true,
		ContentTypeNosniff:    true,
		BrowserXssFilter:      true,
		ContentSecurityPolicy: "default-src 'self'",
		IENoOpen:              true,
		ReferrerPolicy:        "strict-origin-when-cross-origin",
		SSLProxyHeaders:       map[string]string{"X-Forwarded-Proto": "https"},
	})
	// middleware := secure.New(secure.Options{
	// 	SSLRedirect: true,
	// 	SSLHost:     "localhost:5000",
	// })
	return config
	//	err := middleware.process(c.Writer, c.Request)
	// 	if err != nil {
	// 		//If an error occurs, do not continue.
	// 		log.Println(err)
	// 		return
	// 	}
	// 	//Continue processing
	// 	c.Next()
	// }
}
