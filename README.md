# Garden Watch Site

The backend for GardenWatch

### helpful query references
UPDATE tblmeasurement SET measurement = ((measurement / 133.0) * 100) WHERE sensorid='2e';

### To update the backend

1. ssh into the droplet
1. Stop the go server service
  sudo systemctl stop gardenwatchca
1. Build the go server and deploy to server
  back-src/buildDeployProduction.sh
1. Start the go server service
  sudo systemctl start gardenwatchca