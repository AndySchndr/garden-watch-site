import React from 'react';
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'

import { Link } from 'react-router-dom';

const UserCorner: React.FC<{ isSignedIn: boolean }> = ({ isSignedIn }) => {
            /* <div className="flex justify-content-between">
                <Nav.Link className="light-gray as-hover-green"
                    as={Link} to="/about">About</Nav.Link>
                <Nav.Link className="light-gray as-hover-green"
                    as={Link} to="/projects">Projects</Nav.Link>
                <Nav.Link className="light-gray as-hover-green"
                    as={Link} to="/blog">Blog</Nav.Link>
            </div> */
    if(isSignedIn) {
        return (
            <div className="flex justify-between">
                <nav className="flex">
                    <Nav.Link className="light-gray as-hover-green"
                    as={Link} to="/user">Hi, </Nav.Link>
                    {/* <p onClick={() => onRouteChange('signin')}
                        className='f3 link dim black underline pa3 pointer'>Sign Out</p> */}
                </nav>
            </div>
        );
    } else {
        return (
            <div className="flex justify-between">
                <nav className="flex">
                    {/* <p onClick={() => onRouteChange('signin')} className='f3 link dim black underline pa3 pointer'>Sign In</p>
                    <p onClick={() => onRouteChange('register')} className='f3 link dim black underline pa3 pointer'>Register</p> */}
                </nav>
            </div>
        );
    }
}

export default UserCorner;
