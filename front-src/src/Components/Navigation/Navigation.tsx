import React from 'react';
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import Logo from '../../images/Logo.png'

import { Link } from 'react-router-dom';
import './Navigation.scss'

const Navigation: React.FC<{ isSignedIn: boolean }> = ({ isSignedIn }) => {
    return <div id="nav-header" className="hella-h-w-transitions-03 sticky header-bg pv2 ph2">
        <div
            className="flex justify-content-between  mw7-ns mw8-l center">
            <Navbar.Brand className="light-gray as-hover-green"
                as={Link} to="/">
                <img
                    alt=""
                    src={Logo}
                    height="30"
                    object-fit="cover"
                    className="d-inline-block align-top minwid35"
                />{' '}
                <span className="hide-narrow">
                    GardenWatch
                </span>
            </Navbar.Brand>

            {/* Show A search bar instead of GardenWatch depending on the route */}
            {/* Show a user login component in the top right corner */}

            {/* <div className="flex justify-content-between">
                <Nav.Link className="light-gray as-hover-green"
                    as={Link} to="/about">About</Nav.Link>
                <Nav.Link className="light-gray as-hover-green"
                    as={Link} to="/projects">Projects</Nav.Link>
                <Nav.Link className="light-gray as-hover-green"
                    as={Link} to="/blog">Blog</Nav.Link>
            </div> */}
        </div>
    </div>
    // if(isSignedIn) {
    //     return (
    //         <div className="flex justify-between">
    //             <Logo />
    //             <nav className="flex">
    //                 <p onClick={() => onRouteChange('signin')} className='f3 link dim black underline pa3 pointer'>Sign Out</p>
    //             </nav>
    //         </div>
    //     );
    // } else {
    //     return (
    //         <div className="flex justify-between">
    //             <Logo />
    //             <nav className="flex">
    //                 <p onClick={() => onRouteChange('signin')} className='f3 link dim black underline pa3 pointer'>Sign In</p>
    //                 <p onClick={() => onRouteChange('register')} className='f3 link dim black underline pa3 pointer'>Register</p>
    //             </nav>
    //         </div>
    //     );
    // }
}

export default Navigation;
